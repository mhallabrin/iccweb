# ATW - Automation Team Web 

This project is the Quest Diagnostic's starter project for web applications

Background: This is a simple web application that demonstates all the automation testing features.

##  Installing Application

1. Download the source code via git clone of the ATW project in the Quest Automation Team repository

2. npm Install

3. Run or test the app

## Development server

Run `ng serve -- port 4201` for a dev server. Navigate to `http://localhost:4201/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma]

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor]

## Further help


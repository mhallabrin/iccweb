import { browser, by, element } from 'protractor';

export class HomePage {

  getCurrentUrl() {
    return browser.getCurrentUrl();
  }

  getHomeTeam() {
    return element(by.id('homeTeam'));
  }

  getUserRole() {
    return element(by.id('userRole'));
  }

  getUserName() {
    return element(by.id('userName'));
  }

  getNewMessage() {
    return element(by.id('newMessage'));
  }

  getNewMessageBtn() {
    return element(by.id('newMessageBtn'));
  }

  getMessages() {
    return element.all(by.css('p[id^="message"]'));
  }

  getMessage(i) {
    return element(by.id('message' + i));
  }

  getQuestsToDo() {
    return element.all(by.css('p[id^="questsToDo"]'));
  }

  getQuestToDo(i) {
    return element(by.id('questsToDo' + i));
  }

  getQuestsDone() {
    return element.all(by.css('p[id^="questsDone"]'));
  }

  getQuestDone(i) {
    return element(by.id('questsDone' + i));
  }

  getTeamsToDo() {
    return element.all(by.css('p[id^="teamsToDo"]'));
  }

  getTeamToDo(i) {
    return element(by.id('teamsToDo' + i));
  }

  getTeamsDone() {
    return element.all(by.css('p[id^="teamsDone"]'));
  }

  getTeamDone(i) {
    return element(by.id('teamsDone' + i));
  }

  getPersonalsToDo() {
    return element.all(by.css('p[id^="personalsToDo"]'));
  }

  getPersonalToDo(i) {
    return element(by.id('personalsToDo' + i));
  }

  getPersonalsDone() {
    return element.all(by.css('p[id^="personalsDone"]'));
  }

  getPersonalDone(i) {
    return element(by.id('personalsDone' + i));
  }

}

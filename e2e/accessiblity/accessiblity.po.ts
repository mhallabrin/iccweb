import { browser, by, element } from 'protractor';

export class HomePage {

  navigateToLogin() {
    return browser.get('/');
  }

  getLoginUserName() {
    return element(by.id('loginUserName'));
  }

  getLoginPassword() {
    return element(by.id('loginPassword'));
  }

  getLoginSubmitButton() {
    return element(by.id('loginSubmitButton'));
  }

  getHomeTeam() {
    return element(by.id('homeTeam'));
  }
}

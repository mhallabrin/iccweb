import { browser } from 'protractor';
import { runAxeTest } from 'protractor-axe-html-report-plugin';
import { HomePage } from './accessiblity.po';

describe('Hallabrin Family Home page', () => {
  let page: HomePage;

  beforeEach(() => {
    page = new HomePage();
    page.navigateToLogin();
    page.getLoginUserName().sendKeys('Brian');
    page.getLoginPassword().sendKeys('Brian');
    page.getLoginSubmitButton().click();
    expect(page.getHomeTeam()).toEqual('Quest Automation Team');
  });

  it('should pass accessibility tests', function() {
    runAxeTest('Home page');
  });
});

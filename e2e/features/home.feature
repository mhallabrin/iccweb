Feature: Homw Page

    @regression
    Scenario Outline: Home Page validation including sending messages,
                      completing and un-completing items in the todo
                      and done lists.
        Given I am on the home page as <user>
        Then the information on the home page is valid
        When I add a team message
        Then the team message appears
        When I complete the first item in the todo lists
        Then the first item is in the done list
        When I un-complete the first item in the done lists
        Then the first item is in the todo list
    Examples:
    |  user   |
    |  user0  |  # Brian
    |  user1  |  # Kamal
    |  user2  |  # Angela

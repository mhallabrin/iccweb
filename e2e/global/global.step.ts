import {setDefaultTimeout} from 'cucumber';
import { browser, by, element, ExpectedConditions, ElementFinder } from 'protractor';
import { MAT_CHECKBOX_CLICK_ACTION } from '@angular/material';

<<<<<<< HEAD
module.exports = {
  initTest: function() {
    console.log('Global: Resetting timeout to ten seconds');
    setDefaultTimeout(10000);
    console.log('****************************************************');
    console.log(' ');
    console.log('Quest Diagnostics Tests Summary: ');
    console.log('Application: ' + browser.params.app);
    console.log('BaseUrl: ' + browser.baseUrl);
    console.log('Browser: ' + browser.params.browser);
    console.log('Viewport: ' + browser.params.viewport);
    console.log('Runtime: ' + browser.params.runtime);
    console.log('Pipeline: ' + browser.params.pipeline);
    console.log('Test Suite ' + browser.params.test);
    console.log('Date/Time: ' + Date());
    console.log(' ');
    console.log('****************************************************');
  },

  goToURL: async function(url: string, validator: ElementFinder) {
    await browser.get(url);
    await browser.wait(ExpectedConditions.presenceOf(validator), 5000, 'Element taking too long to appear in the DOM');
  },

  click: async function(element: ElementFinder, elementName: String) {
    await browser.wait(ExpectedConditions.presenceOf(element), 5000, 'Element: ' + element + 'not found.');
    await element.isEnabled();
    await element.click();
    console.log('Clicked element: ' + elementName + '.');
  },

  clickRetry: async function(
    element: ElementFinder,
    elementName: String,
    validator: ElementFinder,
    validatorName: String,
    presence: boolean
  ): Promise<void> {
    for (let index = 0; index < 3; index++) {
      await this.click(element, elementName);
      if (presence) {
        console.log('Waiting for presence of validator: ' + validatorName + '.');
        await browser.wait(
          ExpectedConditions.presenceOf(validator),
          8000,
          'Validator: ' + validatorName + ' took too long to appear in the DOM.'
        );
      } else {
        console.log('Waiting for absence of validator: ' + validatorName + '.');
        await browser.wait(
          ExpectedConditions.presenceOf(validator),
          8000,
          'Validator: ' + validatorName + ' took too long to disappear from the DOM.'
        );
      }
    }
  }
};
=======
setDefaultTimeout(20000);
console.log('****************************************************');
console.log(' ');
console.log('Quest Diagnostics Tests Summary: ');
console.log('Application: ' + browser.params.app);
console.log('BaseUrl: ' + browser.baseUrl);
console.log('Browser: ' + browser.params.browser);
console.log('Viewport: ' + browser.params.viewport);
console.log('Runtime: ' + browser.params.runtime);
console.log('Pipeline: ' + browser.params.pipeline);
console.log('Test Suite ' + browser.params.test);
console.log('Date/Time: ' + Date());
console.log(' ');
console.log('****************************************************');
>>>>>>> b977abcae1e914ec9f844212a8692ed0b48ea5b3


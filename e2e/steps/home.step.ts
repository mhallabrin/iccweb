import { HomePage } from '../pages/home.po';
import { LoginPage } from '../pages/login.po';
import { Given, When, Then } from 'cucumber';
import { browser, by, element, ExpectedConditions } from 'protractor';
import * as chai from 'chai';
import * as cap from 'chai-as-promised';
const jsonfile = require('jsonfile');

chai.use(cap);
const expect =  chai.expect;
const EC = ExpectedConditions;

const home: HomePage = new HomePage();
const login: LoginPage = new LoginPage();

let who;

Given(/^I am on the home page as ([^"]*)$/, logon);
  async function logon(user): Promise<void> {
    who = jsonfile.readFileSync('./e2e/json/local/' + user + '.json');
<<<<<<< HEAD
    login.navigateTo();
    await expect(login.getLoginTitle().getText()).to.eventually.equal('Login', 'Login Title does not match');
=======
    await login.navigateTo();
  //  await browser.driver.sleep(3000);
    await expect(login.getLoginTitle().getText()).to.eventually.equal('Login');
>>>>>>> b977abcae1e914ec9f844212a8692ed0b48ea5b3
    await expect(login.getLoginUserLabel().getText()).to.eventually.equal('Username');
    await expect(login.getLoginPasswordLabel().getText()).to.eventually.equal('Password');
    await login.getLoginUserName().sendKeys(who.userName);
    await login.getLoginPassword().sendKeys(who.password);
    await login.getLoginButton().click();
    await expect(home.getHomeTeam().getText()).to.eventually.equal('Quest Automation Team');
  }

Then(/^the information on the home page is valid$/, homePage);
  async function homePage(): Promise<void> {
    await expect(home.getHomeTeam().getText()).to.eventually.equal('Quest Automation Team');
    await expect(home.getUserRole().getText()).to.eventually.equal(who.role);
    await expect(home.getUserName().getText()).to.eventually.equal(who.name);
  }

  When(/^I add a team message$/, addMessage);
  async function addMessage(): Promise<void> {
    await expect(home.getMessages().count()).to.eventually.equal(3);
    await home.getNewMessage().sendKeys('My Team is Awesome');
    await home.getNewMessageBtn().click();
  }

  Then(/^the team message appears$/, checkMessage);
  async function checkMessage(): Promise<void> {
    await expect(home.getMessages().count()).to.eventually.equal(4);
    await browser.wait(EC.textToBePresentInElement(home.getMessage(0), 'Quest Town Hall Meeting scheduled for next Thursday'), 5000);
  }

  When(/^I complete the first item in the todo lists$/, completeTasks);
  async function completeTasks(): Promise<void> {
<<<<<<< HEAD
    await expect(home.getQuestsToDo().count()).to.eventually.equal(who.ctd, 'Pre-Click: Quest ToDo Count Off');
    await expect(home.getQuestsDone().count()).to.eventually.equal(who.cd, 'Pre-Click: Quest Done Count Off');
    await expect(home.getTeamsToDo().count()).to.eventually.equal(who.wtd, 'Pre-Click: Team ToDo Count Off');
    await expect(home.getTeamsDone().count()).to.eventually.equal(who.wd, 'Pre-Click: Team Done Count Off');
    await expect(home.getPersonalsToDo().count()).to.eventually.equal(who.btd, 'Pre-Click: Personal ToDo Count Off');
    await expect(home.getPersonalsDone().count()).to.eventually.equal(who.bd, 'Pre-Click: Personal Done Count Off');
    await browser.wait(EC.elementToBeClickable(home.getQuestToDo(0)), 5000);
    await home.getQuestToDo(0).click();
    await home.getTeamToDo(0).click();
    await home.getPersonalToDo(0).click();
    await browser.wait(ExpectedConditions.presenceOf(home.getQuestDone(who.cd)), 5000, 'Moved Quest Done tasksnot found.');
=======
    await expect(home.getChoresToDo().count()).to.eventually.equal(who.ctd, 'pre-click: chores todo count');
    await expect(home.getChoresDone().count()).to.eventually.equal(who.cd, 'pre-click: chores done count');
    await expect(home.getWorkoutsToDo().count()).to.eventually.equal(who.wtd, 'pre-click: workout todo count');
    await expect(home.getWorkoutsDone().count()).to.eventually.equal(who.wd, 'pre-click: workout done count');
    await expect(home.getBlessingsToDo().count()).to.eventually.equal(who.btd, 'pre-click: blessing todo count');
    await expect(home.getBlessingsDone().count()).to.eventually.equal(who.bd, 'pre-click: blessing done count');
    /*
    if (browser.params.browser === 'chrome') {
      await browser.actions().mouseMove(home.getChoreToDo(0)).perform();
      await browser.driver.sleep(1000);
      await browser.actions().click(home.getChoreToDo(0)).perform();
      await browser.actions().mouseMove(home.getWorkoutToDo(0)).perform();
      await browser.driver.sleep(1000);
      await browser.actions().click(home.getWorkoutToDo(0)).perform();
      await browser.actions().mouseMove(home.getBlessingToDo(0)).perform();
      await browser.driver.sleep(1000);
      await browser.actions().click(home.getBlessingToDo(0)).perform();
    } else {
  */
      await home.getChoreToDo(0).click();
      await home.getWorkoutToDo(0).click();
      await home.getBlessingToDo(0).click();
  //  }

  //  await browser.driver.sleep(6000);
  // click not working in Firefox or chrome for <p> elements
  // browser.actions not working for firefox?
  //


>>>>>>> b977abcae1e914ec9f844212a8692ed0b48ea5b3
  }

  Then(/^the first item is in the done list$/, checkMovedTasks);
  async function checkMovedTasks(): Promise<void> {
<<<<<<< HEAD
    await expect(home.getQuestsToDo().count()).to.eventually.equal(who.ctd - 1, 'Post-Click: Quest ToDo Count Off');
    await expect(home.getQuestsDone().count()).to.eventually.equal(who.cd + 1, 'Post-Click: Quest Done Count Off');
    await expect(home.getTeamsToDo().count()).to.eventually.equal(who.wtd - 1, 'Post-Click: Team ToDo Count Off');
    await expect(home.getTeamsDone().count()).to.eventually.equal(who.wd + 1, 'Post-Click: Team Done Count Off');
    await expect(home.getPersonalsToDo().count()).to.eventually.equal(who.btd - 1, 'Post-Click: Personal ToDo Count Off');
    await expect(home.getPersonalsDone().count()).to.eventually.equal(who.bd + 1, 'Post-Click: Personal Done Count Off');
=======
  //  await browser.wait(EC.presenceOf(home.getChoreToDo(who.cd + 1)), 10000, 'browser click having problems');
    await expect(home.getChoresToDo().count()).to.eventually.equal(who.ctd - 1, 'After click: Chores ToDo count');
    await expect(home.getChoresDone().count()).to.eventually.equal(who.cd + 1, 'After click: Chores Done count');
    await expect(home.getWorkoutsToDo().count()).to.eventually.equal(who.wtd - 1, 'After click: Workouts ToDo count');
    await expect(home.getWorkoutsDone().count()).to.eventually.equal(who.wd + 1, 'After click: Workouts Done count');
    await expect(home.getBlessingsToDo().count()).to.eventually.equal(who.btd - 1, 'After click: Blessings ToDo count');
    await expect(home.getBlessingsDone().count()).to.eventually.equal(who.bd + 1, 'After click: Blessings Done count');
>>>>>>> b977abcae1e914ec9f844212a8692ed0b48ea5b3
  }

  When(/^I un-complete the first item in the done lists$/, unCompleteTask);
  async function unCompleteTask(): Promise<void> {
<<<<<<< HEAD
    await browser.wait(EC.elementToBeClickable(home.getQuestDone(0)), 5000);
    await home.getQuestDone(0).click();
    await home.getTeamDone(0).click();
    await home.getPersonalDone(0).click();
=======
    /*
    if (browser.params.browser === 'chrome') {
      await browser.actions().mouseMove(home.getChoreDone(0)).perform();
      await browser.driver.sleep(1000);
      await browser.actions().click(home.getChoreDone(0)).perform();
      await browser.actions().mouseMove(home.getWorkoutDone(0)).perform();
      await browser.driver.sleep(1000);
      await browser.actions().click(home.getWorkoutDone(0)).perform();
      await browser.actions().mouseMove(home.getBlessingDone(0)).perform();
      await browser.driver.sleep(1000);
      await browser.actions().click(home.getBlessingDone(0)).perform();
    } else {
  */
      await home.getChoreDone(0).click();
      await home.getWorkoutDone(0).click();
      await home.getBlessingDone(0).click();
  //  }
>>>>>>> b977abcae1e914ec9f844212a8692ed0b48ea5b3
  }

  Then(/^the first item is in the todo list$/, checkTasks);

  async function checkTasks(): Promise<void> {
<<<<<<< HEAD
    await expect(home.getQuestsToDo().count()).to.eventually.equal(who.ctd, 'Final-Click: Quest ToDo Count Off');
    await expect(home.getQuestsDone().count()).to.eventually.equal(who.cd, 'Final-Click: Quest Done Count Off');
    await expect(home.getTeamsToDo().count()).to.eventually.equal(who.wtd, 'Final-Click: Team ToDo Count Off');
    await expect(home.getTeamsDone().count()).to.eventually.equal(who.wd, 'Final-Click: Team Done Count Off');
    await expect(home.getPersonalsToDo().count()).to.eventually.equal(who.btd, 'Final-Click: Personal ToDo Count Off');
    await expect(home.getPersonalsDone().count()).to.eventually.equal(who.bd, 'Final-Click: Personal Done Count Off');
=======
    await expect(home.getChoresToDo().count()).to.eventually.equal(who.ctd, 'final click: Chores ToDo count');
    await expect(home.getChoresDone().count()).to.eventually.equal(who.cd, 'final click: Chores done count');
    await expect(home.getWorkoutsToDo().count()).to.eventually.equal(who.wtd, 'final click: workout ToDo count');
    await expect(home.getWorkoutsDone().count()).to.eventually.equal(who.wd, 'final click: workout done count');
    await expect(home.getBlessingsToDo().count()).to.eventually.equal(who.btd, 'final click: blessings ToDo count');
    await expect(home.getBlessingsDone().count()).to.eventually.equal(who.bd, 'final click: blessings done count');
>>>>>>> b977abcae1e914ec9f844212a8692ed0b48ea5b3
  }

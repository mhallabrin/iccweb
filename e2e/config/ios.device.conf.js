// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts
//
//  ng e2e ng e2e --protractor-config ./e2e/config/protractor.conf.js
//

const { SpecReporter } = require('jasmine-spec-reporter');

exports.config = {

  // mjh:  WDIO things for using the selenium server
  //seleniumAddress: 'http://localhost:4444/wd/hub',
  // directConnect: false,
  // end WDIO things

  // mjh: For webdriver manage use below
  directConnect: false,
  // end webdriver manager
  // directConnect set to true to bypass selenium
  // directConnect set to false to use selenium

  allScriptsTimeout: 11000,

 // safari not supported with directConnect
 // Must run selenium standalone
 // and configure Safari to Allow Remote Automtion in Develop Menu

 seleniumAddress: 'http://localhost:4723/wd/hub', //appium

  capabilities: {
    browserName: 'safari',
    platformName: 'iOS',
    automationName: 'XCUITest',

     // iOS Simulator
    platformVersion: '11.2',
    deviceName: 'iPhone Simulator'

 //  iOS Donis iPhone physical device
 //   platformVersion: '11.3.1',   // need upgrade to XCode 9.3+
 //   deviceName: 'Donis iPhone',

//  Physical Device
//    platformVersion: '9.3.5',
//    deviceName: 'CCQJQUSQF4K1',
//    deviceName: 'Michael Touch',

},
//   baseUrl - localhost:4201  for emulator
//   baseUrl - localhost./  for physical device?

  baseUrl: 'http://localhost:4201/',

  // mjh : To run Cucumber run below:
  framework: 'custom',
  specs: [
    '../features/*.feature'
  ],
  // end cucumber

  // mjh: To run Jasime use below:
  //framework: 'jasmine',
  //specs: [
  //  './e2e/**/*.e2e-spec.ts'
  //],
  // end jasmine

  // new cucumber things below: can stay
  frameworkPath: require.resolve('protractor-cucumber-framework'),
  cucumberOpts: {
    compiler: 'ts:ts-node/register',    // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
    require: ['../**/*.step.ts'],  // require step definition files before executing features
    tags: ['@mike'],                           // <string[]> (expression) only execute the features or scenarios with tags matching the expression
    strict: true,                       // <boolean> fail if there are any undefined or pending steps
    'dry-run': false                    // <boolean> invoke formatters without executing steps
  },
  // end new cucumber things

  // old Jasmine things: can stay
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function() {}
  },
  // Future params should come from the command line --params.viewport=md, etc.
  params: {viewport: 'xs',
          browser: 'safari',
          app: 'MyQuest 2.0',
          test: 'Full Regression Suite',
          runtime: 'iPhone Touch',
          pipeline: 'MyQuest On Commit Pipeline'
          },
  onPrepare() {
    console.log('mjh: in onPrepare viewport = ' + browser.params.viewport);
    var width;
    var height;
    switch (browser.params.viewport) {
      case 'xs':
        width = 400;
        height = 600;
        break;
      case 'sm':
        width = 500;
        height = 600;
        break;
      case 'md':
        width = 600;
        height = 600;
        break;
      case 'lg':
        width = 800;
        height = 600;
        break;
      case 'xl':
        width = 900;
        height = 600;
        break;
      default:
        width = 800;
        height = 600;
        break;
    }
//    browser.driver
//      .manage()
//      .window()
//      .setSize(width, height);

    require('ts-node').register({
      project: 'e2e/tsconfig.e2e.json'
    });
//mjh; needed for jasmine, remove for cucumber
//    jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
  }
};

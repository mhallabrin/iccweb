// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts
//
//  ng e2e ng e2e --protractor-config ./e2e/config/protractor.conf.js
//
//     cd ~/Library/Android/Sdk/tools/bin && ./avdmanager list avd
//     cd ~/Library/Android/Sdk/tools && ./emulator -avd Nexus_5X_API_P
//

const { SpecReporter } = require('jasmine-spec-reporter');

exports.config = {

  // mjh:  WDIO things for using the selenium server
  //seleniumAddress: 'http://localhost:4444/wd/hub',
  // directConnect: false,
  // end WDIO things

  // mjh: For webdriver manage use below
  directConnect: false,
  // end webdriver manager
  // directConnect set to true to bypass selenium
  // directConnect set to false to use selenium

  allScriptsTimeout: 11000,

 // safari not supported with directConnect
 // Must run selenium standalone
 // and configure Safari to Allow Remote Automtion in Develop Menu

 seleniumAddress: 'http://localhost:4723/wd/hub', //appium

  capabilities: {
 //   automationName: 'XCUITest',   //not needed
 //  Android notes:
 //
 //  Connecting phone, must be on Setting screen before connecting to Mac
 //
 //  must run appium first with the correct chromedriver
 //  for Galaxy Nexus API 27 - run with chromedriver_2.34
 //  for Mikes phone use chromedriver_2.39
 //     appium --chromedriver-executable /Users/mhallabrin/code/familyWeb/node_modules/protractor/node_modules/webdriver-manager/selenium/chromedriver_2.34
 //
    browserName: 'Chrome',
    //  headless chrome not an option on mobile
    platformName: 'Android',
 //  emulator example
 //   deviceName: 'Galaxy Nexus API 27',
 //   platformVersion: '8.1',

// Physical Device example
  deviceName: 'Mikes',
},
//  ngrok notes:
//  icct.com & R...1!@#$
// at mhallabrin:
//  ./ngrok http 4201 -host-header="localhost:4201"
//

// baseUrl : http:/10.0.2.2:4201 for emulators, ngrok.io for running localhost on web
   baseUrl: 'http://999ed289.ngrok.io',   //
 // baseUrl: 'http://10.0.2.2:4201',

  // mjh : To run Cucumber run below:
  framework: 'custom',
  specs: [
    '../features/*.feature'
  ],
  // end cucumber

  // mjh: To run Jasime use below:
  //framework: 'jasmine',
  //specs: [
  //  './e2e/**/*.e2e-spec.ts'
  //],
  // end jasmine

  // new cucumber things below: can stay
  frameworkPath: require.resolve('protractor-cucumber-framework'),
  cucumberOpts: {
    compiler: 'ts:ts-node/register',    // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
    require: ['../**/*.step.ts'],  // require step definition files before executing features
    tags: ['@mike'],                           // <string[]> (expression) only execute the features or scenarios with tags matching the expression
    strict: true,                       // <boolean> fail if there are any undefined or pending steps
    'dry-run': false                    // <boolean> invoke formatters without executing steps
  },
  // end new cucumber things

  // old Jasmine things: can stay
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function() {}
  },
  // Future params should come from the command line --params.viewport=md, etc.
  params: {viewport: 'xs',
          browser: '',
          app: 'FamilyWeb  2.0',
          test: 'Full Regression Suite',
          runtime: '',
          pipeline: ''
          },
  onPrepare() {
//    browser.params.browser = config.capabilities.browserName;
//    browser.params.runtime = 'Android ' + config.capabilities.deviceName;
    var width;
    var height;
    switch (browser.params.viewport) {
      case 'xs':
        width = 400;
        height = 600;
        break;
      case 'sm':
        width = 500;
        height = 600;
        break;
      case 'md':
        width = 600;
        height = 600;
        break;
      case 'lg':
        width = 800;
        height = 600;
        break;
      case 'xl':
        width = 900;
        height = 600;
        break;
      default:
        width = 800;
        height = 600;
        break;
    }
//    browser.driver
//      .manage()
//      .window()
//      .setSize(width, height);

    require('ts-node').register({
      project: 'e2e/tsconfig.e2e.json'
    });
//mjh; needed for jasmine, remove for cucumber
//    jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
  }
};

//
//  protractor ./e2e/config/chome.conf.js
//

const { SpecReporter } = require('jasmine-spec-reporter');

exports.config = {

  directConnect: false,
  // directConnect set to true to bypass selenium/webdriver-manager
  // directConnect set to false to use selenium/webdriver-manager

  seleniumAddress: 'http://localhost:4444/wd/hub',

// Future params should come from the command line --params.viewport=md, etc.
  params: {viewport: 'lg',    // viewports: xs, sm, md, lg, xl
    browser: 'chrome',
    app: 'familyWeb',
    test: 'Full Regression Suite',
    runtime: 'Local',
    pipeline: 'Upon Request'
},

  capabilities: {
    'browserName': 'chrome'
},

   // Future params should come from the command line --params.viewport=md, etc.
   params: {viewport: 'lg',
    browser: 'chrome',
    app: 'StarterProjectWeb',
    test: 'Full Regression Suite',
    runtime: 'local',
    pipeline: 'Upon Request'
   },

  baseUrl: 'http://localhost:4201/',
//    baseUrl: 'http://localhost:80/',  // running with docker
//    baseUrl: 'http://localhost./',    // running inside with docker

<<<<<<< HEAD
  // 
=======
>>>>>>> b977abcae1e914ec9f844212a8692ed0b48ea5b3
  framework: 'custom',
  specs: [
    '../features/*.feature'
  ],
<<<<<<< HEAD
  //

  // mjh: To run Jasime use below:
  //framework: 'jasmine',
  //specs: [
  //  './e2e/**/*.e2e-spec.ts'
  //],
  // end jasmine
=======
>>>>>>> b977abcae1e914ec9f844212a8692ed0b48ea5b3

  frameworkPath: require.resolve('protractor-cucumber-framework'),
  cucumberOpts: {
    compiler: 'ts:ts-node/register',    // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
    require: ['../**/*.step.ts'],       // require step definition files before executing features
    tags: ['@regression'],                    // <string[]> (expression) only execute the features or scenarios with tags matching the expression
    strict: true,                       // <boolean> fail if there are any undefined or pending steps
    'dry-run': false                    // <boolean> invoke formatters without executing steps
  },

  allScriptsTimeout: 11000,

  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function() {}
  },

  onPrepare() {
    console.log('mjh: in onPrepare viewport = ' + browser.params.viewport);
    var width;
    var height;
    switch (browser.params.viewport) {
      case 'xs':
        width = 450;
        height = 600;
        break;
      case 'sm':
        width = 550;
        height = 600;
        break;
      case 'md':
        width = 700;
        height = 800;
        break;
      case 'lg':
        width = 900;
        height = 800;
        break;
      case 'xl':
        width = 1100;
        height = 850;
        break;
      default:
        width = 800;
        height = 800;
        break;
    }
    browser.driver
      .manage()
      .window()
      .setSize(width, height);

    require('ts-node').register({
      project: 'e2e/tsconfig.e2e.json'
    });
  }
};

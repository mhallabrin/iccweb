// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter } = require('jasmine-spec-reporter');

exports.config = {

  // mjh:  WDIO things for using the selenium server
  //seleniumAddress: 'http://localhost:4444/wd/hub',
  // directConnect: false,
  // end WDIO things

  // mjh: For webdriver manage use below
  directConnect: false,
  // end webdriver manager
  // directConnect set to true to bypass selenium
  // directConnect set to false to use selenium

  seleniumAddress: 'http://localhost:4444/wd/hub',

  localSeleniumStandaloneOpts : {
    jvmArgs : ["-Dwebdriver.ie.driver=//User/michael.j.hallabrin/AppData/Roaming/npm/node_modules\webdriver-manager\selenium\IEDriverServer3.12.0.exe"] // e.g: "node_modules/protractor/node_modules/webdriver-manager/selenium/IEDriverServer_x64_X.XX.X.exe"
  },

  allScriptsTimeout: 11000,
  capabilities: {
    'browserName': 'internet explorer',
        'platform': 'ANY',
        'version': '11',
        'ignoreProtectedModeSettings': true

  },

   // Future params should come from the command line --params.viewport=md, etc.
   params: {viewport: 'lg',
    browser: 'internet explorer',
    app: 'StarterProjectWeb',
    test: 'Full Regression Suite',
    runtime: 'local',
    pipeline: 'Upon Request'
   },

  baseUrl: 'http://localhost:4201/',

  // 
  framework: 'custom',
  specs: [
    '../features/*.feature'
  ],
  //

  // mjh: To run Jasime use below:
  //framework: 'jasmine',
  //specs: [
  //  './e2e/**/*.e2e-spec.ts'
  //],
  // end jasmine

  // new cucumber things below: can stay
  frameworkPath: require.resolve('protractor-cucumber-framework'),
  cucumberOpts: {
    compiler: 'ts:ts-node/register',    // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
    require: ['../steps/*.step.ts'],  // require step definition files before executing features
    tags: [],                           // <string[]> (expression) only execute the features or scenarios with tags matching the expression
    strict: true,                       // <boolean> fail if there are any undefined or pending steps
    'dry-run': false                    // <boolean> invoke formatters without executing steps
  },
  // end new cucumber things

  // old Jasmine things: can stay
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function() {}
  },

  onPrepare() {
    console.log('mjh: in onPrepare viewport = ' + browser.params.viewport);
    var width;
    var height;
    switch (browser.params.viewport) {
      case 'xs':
        width = 400;
        height = 600;
        break;
      case 'sm':
        width = 500;
        height = 600;
        break;
      case 'md':
        width = 600;
        height = 600;
        break;
      case 'lg':
        width = 800;
        height = 600;
        break;
      case 'xl':
        width = 900;
        height = 600;
        break;
      default:
        width = 800;
        height = 600;
        break;
    }
    
    browser.driver
      .manage()
      .window()
      .setSize(width, height);

    require('ts-node').register({
      project: 'e2e/tsconfig.e2e.json'
    });
//mjh; needed for jasmine, remove for cucumber
//    jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
  }
};

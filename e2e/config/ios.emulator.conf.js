//
//
//

const { SpecReporter } = require('jasmine-spec-reporter');

exports.config = {

  directConnect: false,

 seleniumAddress: 'http://localhost:4723/wd/hub', //appium

  capabilities: {
    browserName: 'safari',
    platformName: 'iOS',
    automationName: 'XCUITest',

     // iOS Simulator
    platformVersion: '11.2',
    deviceName: 'iPhone Simulator'

 //  iOS Donis iPhone physical device
 //   platformVersion: '11.3.1',   // need upgrade to XCode 9.3+
 //   deviceName: 'Donis iPhone',

//  Physical Device
//    platformVersion: '9.3.5',
//    deviceName: 'CCQJQUSQF4K1',
//    deviceName: 'Michael Touch',

},
//   baseUrl - localhost:4201  for emulator
//   baseUrl - localhost./  for physical device?

  baseUrl: 'http://localhost:4201/',

  framework: 'custom',
  specs: [
    '../features/*.feature'
  ],

  frameworkPath: require.resolve('protractor-cucumber-framework'),
  cucumberOpts: {
    compiler: 'ts:ts-node/register',    // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
    require: ['../**/*.step.ts'],  // require step definition files before executing features
    tags: ['@regression'],                           // <string[]> (expression) only execute the features or scenarios with tags matching the expression
    strict: true,                       // <boolean> fail if there are any undefined or pending steps
    'dry-run': false                    // <boolean> invoke formatters without executing steps
  },

  allScriptsTimeout: 11000,

  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function() {}
  },
  // Future params should come from the command line --params.viewport=md, etc.
  params: {viewport: 'xs',
          browser: 'safari',
          app: 'MyQuest 2.0',
          test: 'Full Regression Suite',
          runtime: 'iPhone Touch',
          pipeline: 'MyQuest On Commit Pipeline'
          },
  onPrepare() {
    console.log('mjh: in onPrepare viewport = ' + browser.params.viewport);
    var width;
    var height;
    switch (browser.params.viewport) {
      case 'xs':
        width = 400;
        height = 600;
        break;
      case 'sm':
        width = 500;
        height = 600;
        break;
      case 'md':
        width = 600;
        height = 600;
        break;
      case 'lg':
        width = 800;
        height = 600;
        break;
      case 'xl':
        width = 900;
        height = 600;
        break;
      default:
        width = 800;
        height = 600;
        break;
    }
//    browser.driver
//      .manage()
//      .window()
//      .setSize(width, height);

    require('ts-node').register({
      project: 'e2e/tsconfig.e2e.json'
    });
//mjh; needed for jasmine, remove for cucumber
//    jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
  }
};

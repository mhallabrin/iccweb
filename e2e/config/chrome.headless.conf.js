//
//  protractor ./e2e/config/chome.conf.js
//

const { SpecReporter } = require('jasmine-spec-reporter');

exports.config = {

<<<<<<< HEAD
  // mjh:  WDIO things for using the selenium server
  //seleniumAddress: 'http://localhost:4444/wd/hub',
  // directConnect: false,
  // end WDIO things

  // mjh: For webdriver manage use below
  directConnect: true,
  // end webdriver manager
  // directConnect set to true to bypass selenium
  // directConnect set to false to use selenium

  allScriptsTimeout: 11000,
  capabilities: {
    'browserName': 'chrome',
    chromeOptions: {
      args: ["--headless", "--disable-gpu", "--window-size=800,600", "--no-sandbox" ]
=======
  directConnect: false,
  // directConnect set to true to bypass selenium/webdriver-manager
  // directConnect set to false to use selenium/webdriver-manager

  seleniumAddress: 'http://localhost:4444/wd/hub',

// Future params should come from the command line --params.viewport=md, etc.
  params: {viewport: 'na',    // viewports: xs, sm, md, lg, xl
    browser: 'chrome headless',
    app: 'familyWeb',
    test: 'Full Regression Suite',
    runtime: 'Local',
    pipeline: 'Upon Request'
},

capabilities: {
  'browserName': 'chrome',
      chromeOptions: {
          args: ["--headless", "--disable-gpu"]
>>>>>>> b977abcae1e914ec9f844212a8692ed0b48ea5b3
    }
},

<<<<<<< HEAD
   // Future params should come from the command line --params.viewport=md, etc.
   params: {viewport: 'na',
    browser: 'chrome',
    app: 'StarterProjectWeb',
    test: 'Headless Full Regression Suite',
    runtime: 'local',
    pipeline: 'Upon Request'
   },

  baseUrl: 'http://localhost:4201/',

  // 
=======
  baseUrl: 'http://localhost:4201/',
//    baseUrl: 'http://localhost:80/',  // running with docker
//    baseUrl: 'http://localhost./',    // running inside with docker

>>>>>>> b977abcae1e914ec9f844212a8692ed0b48ea5b3
  framework: 'custom',
  specs: [
    '../features/*.feature'
  ],
<<<<<<< HEAD
  //
=======
>>>>>>> b977abcae1e914ec9f844212a8692ed0b48ea5b3

  frameworkPath: require.resolve('protractor-cucumber-framework'),
  cucumberOpts: {
    compiler: 'ts:ts-node/register',    // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
<<<<<<< HEAD
    require: ['../steps/*.step.ts'],  // require step definition files before executing features
    tags: [],                           // <string[]> (expression) only execute the features or scenarios with tags matching the expression
=======
    require: ['../**/*.step.ts'],       // require step definition files before executing features
    tags: ['@regression'],                    // <string[]> (expression) only execute the features or scenarios with tags matching the expression
>>>>>>> b977abcae1e914ec9f844212a8692ed0b48ea5b3
    strict: true,                       // <boolean> fail if there are any undefined or pending steps
    'dry-run': false                    // <boolean> invoke formatters without executing steps
  },

  allScriptsTimeout: 11000,

  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function() {}
  },

  onPrepare() {

    require('ts-node').register({
      project: 'e2e/tsconfig.e2e.json'
    });
  }
};

import { TestBed, inject } from '@angular/core/testing';

import { LoginService } from './login.service';
import { AuthService } from './auth.service';
import { ConfigService } from './config.service';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams, HttpHandler } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

describe('LoginService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoginService,
                AuthService,
                ConfigService,
                HttpClient,
                HttpHandler]
    });
  });

  it('should be created', inject([LoginService], (service: LoginService) => {
    expect(service).toBeTruthy();
  }));
});

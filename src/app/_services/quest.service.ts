import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Quest } from '../_models/quest';
import { ConfigService } from './config.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class QuestService {

  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ) {}

  getQuests(name: string): Observable<Quest[]> {
    return this.http.get<Quest[]>(this.configService.getApiUrl() + 'quests/' + name);
  }

  updateQuest(id: number, status: string) {
    return 'Quest updated';
  }
}

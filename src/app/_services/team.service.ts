import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Team } from '../_models/team';
import { ConfigService } from './config.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class TeamService {

  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ) {}

  getTeams(name: string): Observable<Team[]> {
    return this.http.get<Team[]>(this.configService.getApiUrl() + 'teams/' + name);
  }

  updateTeam(id: number, status: string) {
    return 'Team updated';
  }
}

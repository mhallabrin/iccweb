import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Personal } from '../_models/personal';
import { ConfigService } from './config.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PersonalService {

  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ) {}

  getPersonals(name: string): Observable<Personal[]> {
    return this.http.get<Personal[]>(this.configService.getApiUrl() + 'personals/' + name);
  }

  updatePersonal(id: number, status: string) {
    return 'Personal updated';
  }
}

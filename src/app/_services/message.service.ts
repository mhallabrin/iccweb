import { Injectable } from '@angular/core';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { ConfigService } from './config.service';

@Injectable()
export class MessageService {

  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ) {}

  getMessages() {
    console.log('mike: this should not run');
    return this.http.get<string[]>(this.configService.getApiUrl() + 'messages');
  }

  refreshMessages() {
    return 'Messages Refreshed';
  }

  addMessage(message: string) {
    return 'Messsage Added';
  }

  returnPromise() {
    const promise = new Promise((resolve, reject) => {
      resolve('mjh: promise result');
    });
    return promise;
  }
}


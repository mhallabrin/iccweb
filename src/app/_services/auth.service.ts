import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class AuthService {

  private authenticatedX = new BehaviorSubject<boolean>(false);
  private roleX = new BehaviorSubject<string>('');
  private nameX = new BehaviorSubject<string>('');

  authenticated = this.authenticatedX.asObservable();
  role = this.roleX.asObservable();
  name = this.nameX.asObservable();


  constructor() { }

  setName(name: string) {
    this.nameX.next(name);
  }

  setRole(role: string) {
    this.roleX.next(role);
  }

  setAuth(auth: boolean) {
    this.authenticatedX.next(auth);
  }
}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService} from '../_services/login.service';
import { ConfigService} from '../_services/config.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  model: any = {};

  constructor(
      private route: ActivatedRoute,
      private router: Router,
      private loginService: LoginService,
      private configService: ConfigService
    ) { }

  ngOnInit() {
    this.configService.getConfig();
  }

  login() {

     this.loginService.checkCredentials(this.model.username, this.model.password)
        .then (res => {
          this.model.message = res;
          if (this.model.message === 'Credentials Good') {
            this.router.navigate(['/home']);
          }
       });
  }
}

//  need to fix this syntax and add error condition below:
/*
this.loginService.checkCredentials(this.model.username, this.model.password)
        .then (res => {
          this.model.message = res;
          if (this.model.message === 'Credentials Good') {
            this.router.navigate(['/home']);
          }
        }
       ), (err) =>
        {this.model.message = 'Credentials Not Found, Please try again';
       };
*/

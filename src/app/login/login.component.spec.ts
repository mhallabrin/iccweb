import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router} from '@angular/router';
import { Location } from '@angular/common';
import { RouterTestingModule} from '@angular/router/testing';
import { AppRoutingModule, routes } from '../app-routing.module';
import { FormsModule } from '@angular/forms';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { HomeComponent } from '../home/home.component';
import { LoginComponent } from './login.component';
import { LogoutComponent } from '../logout/logout.component';
import { ConfigService } from '../_services/config.service';
import { LoginService } from '../_services/login.service';
import { AuthService } from '../_services/auth.service';


describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let router: Router;
  let location: Location;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeComponent,
                      LoginComponent,
                      LogoutComponent
                    ],
      imports: [ RouterTestingModule.withRoutes(routes),
                FormsModule
              ],
      providers: [
              LoginService,
              ConfigService,
              AuthService,
              HttpClient,
              HttpHandler
      ],
    })
    .compileComponents();

    router = TestBed.get(Router);
    location = TestBed.get(Location);
    router.initialNavigation();
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router} from '@angular/router';
import { Location } from '@angular/common';
import { RouterTestingModule} from '@angular/router/testing';
import { AppRoutingModule, routes } from '../app-routing.module';
import { FormsModule } from '@angular/forms';
import { trigger, style, transition, animate, keyframes, query, stagger} from '@angular/animations';
import { AuthService} from '../_services/auth.service';
import { MessageService} from '../_services/message.service';
import { QuestService} from '../_services/quest.service';
import { Quest } from '../_models/quest';
import { TeamService} from '../_services/team.service';
import { Team } from '../_models/team';
import { PersonalService} from '../_services/personal.service';
import { Personal } from '../_models/personal';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { ConfigService } from '../_services/config.service';
import { Config } from '../_models/config';
import { HomeComponent } from './home.component';
import { LoginComponent } from '../login/login.component';
import { LogoutComponent } from '../logout/logout.component';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

describe('HomeComponent with Mocks', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let router: Router;
  let location: Location;
  let messageService: MockMessageService;
  let configService: MockConfigService;
  let questService: MockQuestService;
  let teamService: MockTeamService;
  let personalService: MockPersonalService;

  class MockConfigService {
    constructor() {}
    getApiUrl() {
      return '';
    }
  }

  class MockMessageService {
    constructor() {}
    messages = ['Message1', 'Message2', 'Message3'];
    getMessages() {
      return Observable.of(this.messages);
    }
    returnPromise() {
      const promise = new Promise((resolve, reject) => {
        resolve('mjh: Mock promise result');
      });
      return promise;
    }
  }

  class MockQuestService {
    constructor() {}
    quests = [ {'id': 1, 'name': 'Brian', 'desc': 'quest1', 'status': 'todo'},
      {'id': 2, 'name': 'Brian', 'desc': 'quest2', 'status': 'todo'}];
    getQuests() {
      return Observable.of(this.quests);
    }
  }

  class MockPersonalService {
    constructor() {}
    personals = [ {'id': 1, 'name': 'Brian', 'desc': 'personal1', 'status': 'todo'},
      {'id': 2, 'name': 'Brian', 'desc': 'personal2', 'status': 'todo'}];
    getPersonals() {
      return Observable.of(this.personals);
    }
  }

  class MockTeamService {
    constructor() {}
    teams = [ {'id': 1, 'name': 'Brian', 'desc': 'team1', 'status': 'todo'},
      {'id': 2, 'name': 'Brian', 'desc': 'team2', 'status': 'todo'}];
    getTeams() {
      return Observable.of(this.teams);
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeComponent,
                      LoginComponent,
                      LogoutComponent
                    ],
      imports: [ RouterTestingModule.withRoutes(routes),
                FormsModule
              ],
      providers: [
              AuthService,
              [{provide: TeamService, useClass: MockTeamService}],
              [{provide: PersonalService, useClass: MockPersonalService}],
              [{provide: QuestService, useClass: MockQuestService}],
              [{provide: MessageService, useClass: MockMessageService}],
              [{provide: ConfigService, useClass: MockConfigService}],
              HttpClient,
              HttpHandler
      ],
    })
    .compileComponents();
    messageService = new MockMessageService();
    configService = new MockConfigService();
    questService = new MockQuestService();
    teamService = new MockTeamService();
    personalService = new MockPersonalService();

    router = TestBed.get(Router);
    location = TestBed.get(Location);
    router.initialNavigation();
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;

  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('add a message', () => {
    component.ngOnInit();
    expect(component.messages[0]).toBe('Message1');
    expect(component.messages[1]).toBe('Message2');
    expect(component.messages[2]).toBe('Message3');
    component.newMessage = 'Message4';
    component.addMessage();
    expect(component.messages[3]).toBe('Message4');
  });

  it('get Done and Back', () => {
    component.ngOnInit();
    expect(component.quests[0].desc).toBe('quest1');
    expect(component.personals[0].desc).toBe('personal1');
    expect(component.teams[0].desc).toBe('team1');
    expect(component.questsToDo[0]).toBe('quest1');
    expect(component.personalsToDo[0]).toBe('personal1');
    expect(component.teamsToDo[0]).toBe('team1');
    component.questDone(0);
    component.personalDone(0);
    component.teamDone(0);
    expect(component.questsDone[0]).toBe('quest1');
    expect(component.personalsDone[0]).toBe('personal1');
    expect(component.teamsDone[0]).toBe('team1');
    expect(component.questsToDo[0]).toBe('quest2');
    expect(component.personalsToDo[0]).toBe('personal2');
    expect(component.teamsToDo[0]).toBe('team2');
    component.questDone(0);
    component.personalDone(0);
    component.teamDone(0);
    component.questBack(0);
    component.personalBack(0);
    component.teamBack(0);
    expect(component.questsDone[0]).toBe('quest2');
    expect(component.personalsDone[0]).toBe('personal2');
    expect(component.teamsDone[0]).toBe('team2');
    expect(component.questsToDo[0]).toBe('quest1');
    expect(component.personalsToDo[0]).toBe('personal1');
    expect(component.teamsToDo[0]).toBe('team1');
  });

});

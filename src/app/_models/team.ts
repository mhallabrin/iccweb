export interface Team {
    id: number;
    name: string;
    desc: string;
    status: string;
  }

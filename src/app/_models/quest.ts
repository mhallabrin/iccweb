export interface Quest {
    id: number;
    name: string;
    desc: string;
    status: string;
  }

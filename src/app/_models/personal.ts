export interface Personal {
    id: number;
    name: string;
    desc: string;
    status: string;
  }

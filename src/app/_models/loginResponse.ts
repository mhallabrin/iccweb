export interface LoginResponse {
    message: string;
    name: string;
    role: string;
  }

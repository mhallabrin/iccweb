# Stage 0, based on Node.js, to build and compile Angular
FROM node:8.11 as node
WORKDIR /app
COPY package.json /app/
RUN npm install
COPY ./ /app/
ARG env=prod
# RUN npm run build -- --prod --environment $env
RUN npm run build -- --prod
# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:1.13
COPY --from=node /app/dist/familyWeb /usr/share/nginx/html
COPY ./nginx-custom.conf /etc/nginx/conf.d/default.conf

# build for production: docker build -t familyweb:prod .
# build for dev: docker build -t familyweb:dev --build-arg env=dev .

# run for production:  docker run -p 80:80 familyweb:prod
# run for development: docker run -p 80:80 familyweb:dev

